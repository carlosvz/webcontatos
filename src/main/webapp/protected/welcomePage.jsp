<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<script type="text/javascript" src="../resources/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="../resources/js/jquery-ui-1.8.20.js"></script>
<script type="text/javascript" src="../resources/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="../resources/js/bootstrap-carousel.js"></script>



<script type="text/javascript">
	!function($) {
		$(function() {
			// carousel demo
			$('#myCarousel').carousel()
		})
	}(window.jQuery)
</script>

<body>

	<div id="myCarousel" class="carousel slide">
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>
		<!-- Carousel items -->
		<div class="carousel-inner">
			<div class="active item offset3">
				<img src="../resources/img/carrousel/1.png" width="400" height="400"
					alt="img1" >
			</div>
			<div class="item offset3">
				<img class="" src="../resources/img/carrousel/2.png" width="400" height="400"
					" alt="im2">
			</div>
			<div class="item offset3">
				<img src="../resources/img/carrousel/4.png" width="400" height="400"
					alt="img3">
			</div>

		</div>

		<!-- Carousel nav -->
		<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
		<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
	</div>

</body>




