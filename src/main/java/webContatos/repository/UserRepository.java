package webContatos.repository;

import org.springframework.data.repository.CrudRepository;
import webContatos.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findByEmail(String email);
}
