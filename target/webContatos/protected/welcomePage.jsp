<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript"> src="../resources/bootstrap-carrousel.js"></script>

<div class="row-fluid">
	<div class="jumbotron">
		<h1>
			<spring:message code='contatos' />
		</h1>

		<p class="lead">
			<spring:message code='welcomePage.description' />
		</p>
	</div>
</div>

<body>

	<div id="myCarousel" class="carousel slide">
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>
		<!-- Carousel items -->
		<div class="carousel-inner">
			<div class="active item">
				<img src="../resources/img/carrousel/1.png" width="400" height="400" alt="img1">
				
				</div>
			</div>
			<div class="item">
				<img src="../resources/img/carrousel/2.png" width="400" height="400"" alt="im2">
				<div class="container">
				
			</div>
			<div class="item">
				<img src="../resources/img/carrousel/4.png" width="400" height="400" alt="img3">			
			</div>

		</div>

		<!-- Carousel nav -->
		<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
		<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
	</div>

</body>


